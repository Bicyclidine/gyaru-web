import React from 'react'

import EditMediaForm from '../../components/EditMediaForm/EditMediaForm.js'
import PleaseWait from '../../components/PleaseWait/PleaseWait.js'

import MediaApi from '../../api/Media.js'

// writeme
class EditMediaPage extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      media: null
    }

    this.handleSubmit = this.handleSubmit.bind(this)
  }

  componentDidMount () {
    MediaApi.show(this.props.id).then(
      json => this.setState({ media: json }),
      this.props.onError
    )
  }

  handleSubmit (params) {
    MediaApi.update(this.props.token, this.props.id, params).then(
      () => this.props.onPageChange('media', this.props.id),
      this.props.onError
    )
  }

  render () {
    const e = React.createElement

    if (!this.state.media) { return e(PleaseWait) }

    return e('div', { className: 'edit-media-page' },
      e(EditMediaForm, {
        media: this.state.media,
        onSubmit: this.handleSubmit,
        onCancel: () => this.props.onPageChange('media', this.props.id)
      })
    )
  }
}

export default EditMediaPage
