import React from 'react'

import StatusPostList from '../../components/StatusPostList/StatusPostList.js'

function MainPage (props) {
  const e = React.createElement

  return (
    e(StatusPostList, props)
  )
}

export default MainPage
