import React from 'react'

import EditMediaForm from '../../components/EditMediaForm/EditMediaForm.js'

import MediaApi from '../../api/Media.js'

// writeme
class CreateMediaPage extends React.Component {
  constructor (props) {
    super(props)

    this.state = {}

    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleSubmit (params) {
    MediaApi.create(this.props.token, params).then(
      json => this.props.onPageChange('media', json.id),
      this.props.onError
    )
  }

  render () {
    const e = React.createElement

    return e('div', { className: 'create-media-page' },
      e(EditMediaForm, {
        onSubmit: this.handleSubmit,
        onCancel: () => this.props.onPageChange('media')
      })
    )
  }
}

export default CreateMediaPage
