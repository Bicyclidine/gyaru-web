import React from 'react'
import './UserDataPage.css'

import SimpleButton from '../../components/SimpleButton/SimpleButton.js'
import ListImportRow from '../../components/ListImportRow/ListImportRow.js'
import PleaseWait from '../../components/PleaseWait/PleaseWait.js'

import ListEntryApi from '../../api/ListEntry.js'

// Data import/export stuff.
class UserDataPage extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      loading: false,
      file: null,
      results: null
    }

    this.handleFileChange = this.handleFileChange.bind(this)
    this.handleAnilistUpload = this.handleAnilistUpload.bind(this)
    this.handleClearList = this.handleClearList.bind(this)
    this.handleDataDownload = this.handleDataDownload.bind(this)
  }

  handleFileChange (event) {
    this.setState({ file: event.target.files[0] })
  }

  handleAnilistUpload () {
    this.setState({ loading: true })
    ListEntryApi.uploadAnilistData(this.props.token, this.state.file).then(
      json => this.setState({ loading: false, file: null, results: json }),
      json => {
        this.setState({ loading: false, file: null }) // Probably a bad file...
        this.props.onError(json)
      }
    )
  }

  handleClearList (kind) {
    if (!window.confirm(`Clear all items on your ${kind} list?`)) { return }

    this.setState({ loading: true })
    ListEntryApi.clear(this.props.token, { kind: kind }).then(
      json => this.setState({ loading: false }),
      json => {
        this.setState({ loading: false })
        this.props.onError(json)
      }
    )
  }

  handleDataDownload () {
    const params = { user: this.props.currentUser.id }

    this.setState({ loading: true })
    ListEntryApi.all(params).then(
      json => {
        this.setState({ loading: false })

        const data = JSON.stringify(json, null, 2)

        const a = document.createElement('a')
        a.download = 'export.json'
        a.href = 'data:application/json,' + encodeURI(data)
        a.click()
      },
      json => {
        this.setState({ loading: false })
        this.props.onError(json)
      }
    )
  }

  render () {
    const e = React.createElement

    if (this.state.loading) { return e(PleaseWait) }

    const results = this.state.results ? Object.keys(this.state.results).map(
      (name, idx) => (
        e(ListImportRow, {
          key: idx,
          name: name,
          data: this.state.results[name]
        })
      )
    ) : null

    return e('div', { className: 'user-data-page' },
      e('div', { className: 'user-data-page-upload' },
        e('input', {
          className: 'user-data-page-file',
          type: 'file',
          accept: 'application/json',
          onChange: this.handleFileChange
        }),
        e(SimpleButton, {
          theme: 'primary',
          disabled: !this.state.file,
          onClick: this.handleAnilistUpload
        }, 'Upload')
      ),
      e('div', { className: 'user-data-page-clear' },
        e(SimpleButton, {
          theme: 'delete',
          onClick: () => this.handleClearList('anime')
        }, 'Clear anime list'),
        e(SimpleButton, {
          theme: 'delete',
          onClick: () => this.handleClearList('manga')
        }, 'Clear manga list')
      ),
      e('div', { className: 'user-data-page-export' },
        e(SimpleButton, {
          theme: 'create',
          onClick: this.handleDataDownload
        }, 'Download lists')
      ),
      e('div', { className: 'user-data-page-results' }, results)
    )
  }
}

export default UserDataPage
