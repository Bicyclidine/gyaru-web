// Picks the "preferred" name from a list of possible alternatives.
function localName (media) {
  // TODO: Pick the non-JP language based on the user settings, if logged in.
  const englishName = media.names.find(n => n.language === 'en')
  // TODO: Default to Hangul (for example) for Korean animations instead of JP?
  const romajiName = media.names.find(n => n.variant === 'romaji')
  const kanjiName = media.names.find(n => n.variant === 'kanji')
  const kanaName = media.names.find(n => n.variant === 'kana')

  // TODO: Prioritise the "preferred" name (API search is currently JP-based).
  return (romajiName || englishName || kanaName || kanjiName || {}).text
}

export default localName
