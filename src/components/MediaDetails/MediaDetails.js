import React from 'react'
import './MediaDetails.css'

import MarkdownContent from '../../components/MarkdownContent/MarkdownContent.js'
import MediaTag from '../../components/MediaTag/MediaTag.js'

// writeme
function MediaDetails (props) {
  const e = React.createElement

  const counter = props.media.kind === 'manga' ? 'chapter' : 'episode'

  const tags = props.media.mediaTags.map(mediaTag => (
    e(MediaTag, { key: mediaTag.id, mediaTag: mediaTag })
  ))

  return (
    e('div', { className: 'media-details' },
      e('div', { className: 'media-details-status' },
        e('div', { className: 'media-details-status-label' },
          'Status'
        ),
        e('div', { className: 'media-details-status-value' },
          props.media.status
        )
      ),

      props.media.episodes ? (
        e('div', { className: 'media-details-episodes' },
          e('div', { className: 'media-details-episodes-count' },
            props.media.episodes
          ),
          e('div', { className: 'media-details-episodes-units' },
            props.media.episodes === 1 ? counter : `${counter}s`
          )
        )
      ) : null,

      props.media.studio ? (
        e('div', { className: 'media-details-studio' },
          e('div', { className: 'media-details-studio-label' },
            'Studio'
          ),
          e('div', { className: 'media-details-studio-value' },
            props.media.studio.name
          )
        )
      ) : null,

      props.media.volumes ? (
        e('div', { className: 'media-details-volumes' },
          e('div', { className: 'media-details-volumes-count' },
            props.media.volumes
          ),
          e('div', { className: 'media-details-volumes-units' },
            props.media.volumes === 1 ? 'volume' : 'volumes'
          )
        )
      ) : null,

      e('div', { className: 'media-details-description' },
        e(MarkdownContent, { text: props.media.about })
      ),

      e('div', { className: 'media-details-tags' }, tags)
    )
  )
}

export default MediaDetails
