import React from 'react'
import { render } from '@testing-library/react'
import ErrorMessage from './ErrorMessage'

it('returns null if no message is provided', () => {
  const result = render(<ErrorMessage message="" />)

  expect(result.container.querySelector('.error-message')).toBeNull()
})

it('renders correctly if a message is provided', () => {
  const result = render(<ErrorMessage message="Hello, world!" />)

  expect(result.container.querySelector('.error-message')).toBeTruthy()
  expect(result).toMatchSnapshot()
})
