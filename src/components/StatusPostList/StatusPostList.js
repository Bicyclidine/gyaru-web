import React from 'react'
import './StatusPostList.css'

import PleaseWait from '../PleaseWait/PleaseWait.js'
import StatusPostListItem from '../StatusPostListItem/StatusPostListItem.js'
import StatusPostEditor from '../StatusPostEditor/StatusPostEditor.js'

import StatusPostApi from '../../api/StatusPost.js'

// writeme
class StatusPostList extends React.Component {
  constructor (props) {
    super(props)

    this.state = { posts: null }

    this.fetchPosts = this.fetchPosts.bind(this)
    this.handleCreatePost = this.handleCreatePost.bind(this)
    this.handleUpdatePost = this.handleUpdatePost.bind(this)
  }

  componentDidMount () {
    this.fetchPosts()
  }

  handleCreatePost (params) {
    StatusPostApi.create(this.props.token, params).then(
      () => {
        this.setState({ postSubmittedAt: Date() }) // Force-reset the editor.
        this.fetchPosts() // We COULD just append this new post, but...
      },
      this.props.onError
    )
  }

  // This can happen if a post is liked/unliked.
  handleUpdatePost (json) {
    const posts = this.state.posts.map(post => post.id === json.id ? json : post)
    this.setState({ posts: posts })
  }

  fetchPosts () {
    const params = this.props.userId ? { user: this.props.userId } : null

    StatusPostApi.all(this.props.token, params).then(
      json => this.setState({ posts: json.items }),
      this.props.onError
    )
  }

  render () {
    const e = React.createElement

    const editor = this.props.token && !this.props.userId ? (
      e(StatusPostEditor, {
        key: this.state.postSubmittedAt,
        onSubmit: this.handleCreatePost
      })
    ) : null

    const posts = this.state.posts ? (
      this.state.posts.map(post => (
        e(StatusPostListItem, {
          key: post.id,
          post: post,
          token: this.props.token,
          currentUser: this.props.currentUser,
          showReplyCount: true,
          onError: this.props.onError,
          onPageChange: this.props.onPageChange,
          onUpdate: this.handleUpdatePost,
          onDestroy: this.fetchPosts
        })
      ))
    ) : (
      e(PleaseWait)
    )

    return (
      e('div', { className: 'status-post-list' },
        e('div', { className: 'status-post-list-head' }, editor),
        e('div', { className: 'status-post-list-body' }, posts)
      )
    )
  }
}

export default StatusPostList
