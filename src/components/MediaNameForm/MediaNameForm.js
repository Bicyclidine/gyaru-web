import React from 'react'
import './MediaNameForm.css'

import SimpleButton from '../SimpleButton/SimpleButton.js'
import SimpleDropdown from '../SimpleDropdown/SimpleDropdown.js'
import SimpleInput from '../SimpleInput/SimpleInput.js'

// Represents a single "name" row on the Edit Media form.
function MediaNameForm (props) {
  const e = React.createElement

  if (props.name._destroy) { return null }

  return (
    e('div', { className: 'media-name-form' },
      e('div', { className: 'media-name-form-text' },
        e(SimpleInput, {
          value: props.name.text,
          onChange: props.onTextChange
        })
      ),
      e('div', { className: 'media-name-form-language' },
        e(SimpleDropdown, {
          value: props.name.language,
          options: ['en', 'jp'],
          onChange: props.onLanguageChange
        })
      ),
      e('div', { className: 'media-name-form-variant' },
        e(SimpleDropdown, {
          value: props.name.variant,
          options: ['kanji', 'kana', 'romaji'], // TODO: Fetch via API.
          disabled: props.name.language !== 'jp',
          onChange: props.onVariantChange
        })
      ),
      e(SimpleButton, { theme: 'delete', onClick: props.onRemove }, 'Remove')
    )
  )
}

export default MediaNameForm
