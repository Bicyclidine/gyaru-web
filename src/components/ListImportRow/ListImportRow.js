import React from 'react'
import './ListImportRow.css'

// writeme
function ListImportRow (props) {
  const e = React.createElement

  const okay = !props.data.errors.length

  return (
    e('div', { className: `list-import-row ${okay ? 'okay' : 'fail'}` },
      e('div', { className: 'list-import-row-kind' },
        props.data.kind
      ),
      e('div', { className: 'list-import-row-name' },
        props.name
      ),
      e('div', { className: 'list-import-row-status' },
        okay ? 'Success' : 'Failure'
      ),
      e('div', { className: 'list-import-row-errors' },
        props.data.errors.join(' / ')
      )
    )
  )
}

export default ListImportRow
