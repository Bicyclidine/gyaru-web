import React from 'react'
import { render } from '@testing-library/react'
import PageFooter from './PageFooter'

it('renders correctly', () => {
  const result = render(<PageFooter />)

  expect(result).toMatchSnapshot()
})
