import React from 'react'
import './EditMediaForm.css'

import SimpleButton from '../SimpleButton/SimpleButton.js'
import SimpleCheckbox from '../SimpleCheckbox/SimpleCheckbox.js'
import InputLabel from '../InputLabel/InputLabel.js'
import ImagePicker from '../ImagePicker/ImagePicker.js'
import MarkdownEditor from '../MarkdownEditor/MarkdownEditor.js'
import MediaNameForm from '../MediaNameForm/MediaNameForm.js'
import SimpleDropdown from '../SimpleDropdown/SimpleDropdown.js'
import SimpleInput from '../SimpleInput/SimpleInput.js'

import { mediaKinds, mediaStatuses } from '../../helpers/commonEnums.js'

import StudioApi from '../../api/Studio.js'

// writeme
class EditMediaForm extends React.Component {
  constructor (props) {
    super(props)

    const names = props.media ? props.media.names.map(name => ({
      id: name.id,
      text: name.text,
      language: name.language,
      variant: name.variant || '',
      _destroy: false
    })) : [this.newName()]

    const media = props.media || {}

    this.state = {
      names: names,
      status: media.status || mediaStatuses[0],
      episodes: media.episodes || '',
      volumes: media.volumes || '',
      anidbCode: media.anidbCode || '',
      anilistCode: media.anilistCode || '',
      animePlanetCode: media.animePlanetCode || '',
      kitsuCode: media.kitsuCode || '',
      malCode: media.malCode || '',
      adult: media.adult || false,
      kind: media.kind || mediaKinds[0],
      studioName: (media.studio || {}).name || '',
      studios: {},
      picture: null
    }

    this.handleAddName = this.handleAddName.bind(this)
    this.handleRemoveName = this.handleRemoveName.bind(this)

    this.handleNameTextChange = this.handleNameTextChange.bind(this)
    this.handleNameLanguageChange = this.handleNameLanguageChange.bind(this)
    this.handleNameVariantChange = this.handleNameVariantChange.bind(this)

    this.handleStudioChange = this.handleStudioChange.bind(this)
    this.handleStatusChange = this.handleStatusChange.bind(this)
    this.handleEpisodesChange = this.handleEpisodesChange.bind(this)
    this.handleVolumesChange = this.handleVolumesChange.bind(this)
    this.handleAnidbCodeChange = this.handleAnidbCodeChange.bind(this)
    this.handleAnilistCodeChange = this.handleAnilistCodeChange.bind(this)
    this.handleAnimePlanetCodeChange = this.handleAnimePlanetCodeChange.bind(this)
    this.handleKitsuCodeChange = this.handleKitsuCodeChange.bind(this)
    this.handleMalCodeChange = this.handleMalCodeChange.bind(this)
    this.handlePictureChange = this.handlePictureChange.bind(this)
    this.handleAdultToggle = this.handleAdultToggle.bind(this)
    this.handleKindChange = this.handleKindChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  newName () {
    return {
      text: '',
      language: 'en',
      variant: '',
      _destroy: false
    }
  }

  componentDidMount () {
    StudioApi.all().then(
      json => {
        const nameToId = {}
        json.items.forEach(studio => { nameToId[studio.name] = studio.id })
        this.setState({ studios: nameToId })
      },
      this.props.onError
    )
  }

  handleAddName () {
    this.setState({ names: this.state.names.concat(this.newName()) })
  }

  handleRemoveName (index) {
    const newNames = this.state.names.map((name, num) => (
      index === num ? { ...name, _destroy: true } : name
    ))

    this.setState({ names: newNames })
  }

  handleNameTextChange (index, value) {
    const newNames = this.state.names.map((name, num) => (
      index === num ? { ...name, text: value } : name
    ))

    this.setState({ names: newNames })
  }

  handleNameLanguageChange (index, value) {
    const newNames = this.state.names.map((name, num) => (
      index === num ? { ...name, language: value, variant: '' } : name
    ))

    this.setState({ names: newNames })
  }

  handleNameVariantChange (index, value) {
    const newNames = this.state.names.map((name, num) => (
      index === num ? { ...name, variant: value } : name
    ))

    this.setState({ names: newNames })
  }

  handleStudioChange (value) { this.setState({ studioName: value }) }
  handleStatusChange (value) { this.setState({ status: value }) }
  handleEpisodesChange (event) { this.setState({ episodes: event.target.value }) }
  handleVolumesChange (event) { this.setState({ volumes: event.target.value }) }
  handleAnidbCodeChange (event) { this.setState({ anidbCode: event.target.value }) }
  handleAnilistCodeChange (event) { this.setState({ anilistCode: event.target.value }) }
  handleAnimePlanetCodeChange (event) { this.setState({ animePlanetCode: event.target.value }) }
  handleKitsuCodeChange (event) { this.setState({ kitsuCode: event.target.value }) }
  handleMalCodeChange (event) { this.setState({ malCode: event.target.value }) }
  handlePictureChange (file) { this.setState({ picture: file }) }
  handleAdultToggle () { this.setState({ adult: !this.state.adult }) }

  handleKindChange (value) {
    if (value === 'manga') {
      this.setState({ kind: value, anidbCode: '', studioName: '' })
    } else {
      this.setState({ kind: value, volumes: '' })
    }
  }

  handleSubmit (text) {
    const params = { ...this.state, about: text }

    // The API wants a studio ID, rather than a name, so:
    delete params.studios
    delete params.studioName
    params.studioId = this.state.studios[this.state.studioName] || ''

    // The API uses a different label for the "names" array:
    delete params.names
    params.namesAttributes = this.state.names

    // Only include a "picture" attribute if a new image has been specified:
    if (!params.picture) { delete params.picture }

    this.props.onSubmit(params)
  }

  render () {
    const e = React.createElement

    const kind = this.state.kind // Some details differ for anime/manga.

    const names = this.state.names.map((name, idx) => (
      e(MediaNameForm, {
        key: idx,
        name: name,
        onTextChange: (e) => this.handleNameTextChange(idx, e.target.value),
        onLanguageChange: (value) => this.handleNameLanguageChange(idx, value),
        onVariantChange: (value) => this.handleNameVariantChange(idx, value),
        onRemove: () => this.handleRemoveName(idx)
      })
    ))

    return (
      e('div', { className: 'edit-media-form' },
        e('div', { className: 'edit-media-form-picture' },
          e(ImagePicker, {
            width: 240,
            height: 360,
            fileSize: '3MB',
            onChange: this.handlePictureChange
          })
        ),
        e('div', { className: 'edit-media-form-name' },
          e(InputLabel, { label: 'Names' },
            e('div', { className: 'edit-media-form-name-list' }, names)
          )
        ),
        e('div', { className: 'edit-media-form-kind' },
          e(InputLabel, { label: 'Kind' },
            e(SimpleDropdown, {
              value: this.state.kind,
              options: mediaKinds,
              onChange: this.handleKindChange
            })
          )
        ),
        e('div', { className: 'edit-media-form-status' },
          e(InputLabel, { label: 'Status' },
            e(SimpleDropdown, {
              value: this.state.status,
              options: mediaStatuses,
              onChange: this.handleStatusChange
            })
          )
        ),
        e('div', { className: 'edit-media-form-studio' },
          e(InputLabel, { label: 'Studio' },
            e(SimpleDropdown, {
              value: this.state.studioName,
              options: Object.keys(this.state.studios),
              allowClear: true,
              disabled: kind !== 'anime',
              onChange: this.handleStudioChange
            })
          )
        ),
        e('div', { className: 'edit-media-form-episodes' },
          e(InputLabel, { label: kind === 'manga' ? 'Chapters' : 'Episodes' },
            e(SimpleInput, {
              type: 'number',
              min: 1,
              value: this.state.episodes,
              onChange: this.handleEpisodesChange
            })
          )
        ),
        e('div', { className: 'edit-media-form-volumes' },
          e(InputLabel, { label: 'Volumes' },
            e(SimpleInput, {
              type: 'number',
              min: 1,
              disabled: kind !== 'manga',
              value: this.state.volumes,
              onChange: this.handleVolumesChange
            })
          )
        ),
        e('div', { className: 'edit-media-form-site-anidb' },
          e(InputLabel, { label: 'AniDB ID' },
            e(SimpleInput, {
              disabled: kind !== 'anime',
              value: this.state.anidbCode,
              onChange: this.handleAnidbCodeChange
            })
          )
        ),
        e('div', { className: 'edit-media-form-site-anilist' },
          e(InputLabel, { label: 'Anilist ID' },
            e(SimpleInput, {
              value: this.state.anilistCode,
              onChange: this.handleAnilistCodeChange
            })
          )
        ),
        e('div', { className: 'edit-media-form-site-anime-planet' },
          e(InputLabel, { label: 'Anime Planet ID' },
            e(SimpleInput, {
              value: this.state.animePlanetCode,
              onChange: this.handleAnimePlanetCodeChange
            })
          )
        ),
        e('div', { className: 'edit-media-form-site-kitsu' },
          e(InputLabel, { label: 'Kitsu ID' },
            e(SimpleInput, {
              value: this.state.kitsuCode,
              onChange: this.handleKitsuCodeChange
            })
          )
        ),
        e('div', { className: 'edit-media-form-site-mal' },
          e(InputLabel, { label: 'My Anime List ID' },
            e(SimpleInput, {
              value: this.state.malCode,
              onChange: this.handleMalCodeChange
            })
          )
        ),
        e('div', { className: 'edit-media-form-about' },
          e(InputLabel, { label: 'Description' },
            e(MarkdownEditor, {
              initialText: (this.props.media || {}).about,
              onSubmit: this.handleSubmit
            },
              e('div', { className: 'edit-media-form-markdown-editor-footer' },
                e(SimpleCheckbox, {
                  label: '18+ Content',
                  checked: this.state.adult,
                  onChange: this.handleAdultToggle
                }),
                e(SimpleButton, { theme: 'create', onClick: this.handleAddName },
                  'Add name'
                ),
                e(SimpleButton, { onClick: this.props.onCancel }, 'Cancel')
              )
            )
          )
        )
      )
    )
  }
}

export default EditMediaForm
