import React from 'react'
import './MediaCard.css'

import RemoteImage from '../RemoteImage/RemoteImage.js'

import localName from '../../helpers/localName.js'

// Used on the browse page.
function MediaCard (props) {
  const e = React.createElement

  return (
    e('div', { className: 'media-card' },
      e('button', { className: 'title', onClick: props.onClick },
        localName(props.media)
      ),
      e('div', { className: 'image-container' },
        e(RemoteImage, {
          className: 'image',
          remotePath: props.media.picture,
          fallback: 'no-thumbnail.png',
          alt: 'Thumbnail'
        })
      ),
      e('div', { className: 'kind' }, props.media.kind)
    )
  )
}

export default MediaCard
