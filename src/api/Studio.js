import apiCall from './apiCall.js'
import buildGetQuery from './buildGetQuery.js'
import buildFormData from './buildFormData.js'

// Handles all studio requests.
class Studio {
  static all (params) {
    const data = buildGetQuery(params)
    return apiCall('GET', 'api/v1/studios', null, data)
  }

  static show (id) {
    return apiCall('GET', `api/v1/studios/${id}`)
  }

  static create (token, params) {
    const data = buildFormData('studio', params)
    return apiCall('POST', 'api/v1/studios', token, data)
  }

  static update (token, id, params) {
    const data = buildFormData('studio', params)
    return apiCall('PATCH', `api/v1/studios/${id}`, token, data)
  }

  static destroy (token, id) {
    return apiCall('DELETE', `api/v1/studios/${id}`, token)
  }
}

export default Studio
