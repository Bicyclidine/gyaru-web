GYARU Web Client
================

A NodeJS-based evolution of the original "test" client. Still needs work!

Looking for the back-end Rails API? <https://gitlab.com/gobusto/gyaru-api/>

Quick start
-----------

Create a `.env` file, and define `REACT_APP_API_SERVER` like so:

    REACT_APP_API_SERVER=http://127.0.0.1:3000

...and then run `PORT=5000 npm start` to get things up and running.

Use `CI=true npm test` to run tests or `npx eslint ./src` to check the code for
style errors.

TODO
----

This is basically just for my own reference, so that I don't forget anything:

+ Add more-interesting CSS for the `PleaseWait` component.
+ Maybe show recent reviews etc. on the "main" page?
+ Show user/media icons on reviews (depending on where they're shown)?
+ Show media icons on list entries?
+ Add React error-boundary components!
+ Paginate status post lists.
+ Replace the ANIME/MANGA text with chapters/volumes on MediaCard components.
+ Improve the CSS on the import/export page.
+ Use React Router.
+ Change the page title when the user navigates around.

Markdown Notes
--------------

Formatting is done by [CommonMark](https://github.com/commonmark/commonmark.js)
with the `safe` option enabled, meaning raw HTML is not allowed - this prevents
potential security problems such as `<script>` tags in status posts. It is also
set up to handle newlines as literal newlines (`softbreak: '<br>'`), as this is
how most people expect things to work.

Since CommonMark is a rather "vanilla" Markdown variant, there are a few things
which it does not support:

+ Tables (Supported by Reddit and Github)
+ Spoilers (Supported by Reddit/Anilist/StackExchange, with different syntaxes)
+ Youtube embeds (Supported by Anilist via `youtube()`)
+ Video file embeds (Supported by Anilist via `webm()`)
+ Other magic links (On Anilist, links to anime/manga pages are auto-converted)
+ Sized images (Anilist)
+ Strikethrough text (Anilist)
+ Right-aligned/centred/justified text (Anilist)
+ Superscript (Supported by Reddit)

Additionally, literal URLs such as http://example.com are not auto-linked; they
must be surrounded with < and > characters.

Spoilers aren't much of a problem, since GYARU allows status posts to be marked
as "spoilers" via a checkbox. Some of the other features would be nice-to-have,
though they would need to be implemented carefully; a lot of the extra features
supported by Anilist are rather buggy.

For example, using `img200(link)` within a code block on Anilist will result in
`<img width='200' src='link'>` being generated because these extra features are
implemented as a basic search-and-replace _after_ the Markdown parsing happens.
A better approach would be to replace `<a>` tags in the generated DOM tree (not
the raw HTML text) based on the contents of their `href` attribute - this would
be much more reliable for things such as Youtube (or Twitter) embeds.

For sized images, we could use the `alt` text of generated `<img>` tags (again,
not the raw HTML) as a width value if it only contains numeric characters. This
avoids introducing a special syntax, and would work reliably, though it ignores
the intended purpose of `alt` text (accessibility) as a consequence.

Other features (strikethrough/superscript/aligned text and tables) are probably
possible via [XPath](https://developer.mozilla.org/en-US/docs/Web/XPath), if it
were necessary to implement them.

For reference, here are the various "flavours" of Markdown:

+ Markdown: https://daringfireball.net/projects/markdown/
+ CommonMark: https://commonmark.org/
+ Github: https://github.github.com/gfm/
+ Reddit: https://www.reddit.com/wiki/markdown
+ Anilist: https://anilist.co/forum/thread/6125
+ StackExchange: https://stackoverflow.com/editing-help

License
-------

Copyright (c) 2020 Thomas Glyn Dennis

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
